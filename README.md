## About 
-------------

O IFAccess é um aplicativo para auxilio a pessoas com deficiência visual (cegueira e/ou baixa visão). O objetivo do app é ajudar o usuário a se localizar dentro do campus IFBA Salvador, apresentando onde a pessoa está ou em qual prédio encontra-se e suas informações.


## Android Settings
-------------

- Google Play Services: para o correto funcionamento da aplicação que denpende dos recursos do google service, é necessária as seguintes configurações no arquivo gradle em `/app/App_Resources/Android/app.gradle`

```sh
project.ext {
    googlePlayServicesVersion = "+"
}

dependencies {
  implementation "androidx.multidex:multidex:2.0.1"
  def googlePlayServicesVersion = project.googlePlayServicesVersion
  compile "com.google.android.gms:play-services-location:$googlePlayServicesVersion"
}
```

## Commands
-------------

- Iniciar a aplicação: utilize um emulador android (_exemplo: genymotion_)
```sh
npm i
tns run android --no-hmr
```

- Rodar testes e2e

```sh
npm run e2e
```
## tasks

- Trocar icones default do google maps para o que está na pasta imagens (o source estava quebrando ao fazer icon.sourceImagem = icon - entender o porque e como trocar)

- adicionar ao marker um evento de clique para levar a uma nova página com descritivo total do texto (abertura de modal)

- redução do botão 'relocalizar' e adicionar outro botão de 'informações sobre os prédios' (já existe em partes, rpecisa ajustar o CSS) 
